<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1</title>
</head>
<body>

	<!-- Activity 1 -->

	<h1>Full Address:</h1>

	<p><?php echo getFullAddress('Philippines', 'Quezon City', 'Metro Manila', '18F Bldg C Grass Residences')   ?></p>

	<!-- Activity 2 -->

	<h1>Letter-Based Grading</h1>

	<p><?php echo getLetterGrade(87) ?></p>
	<p><?php echo getLetterGrade(94) ?></p>
	<p><?php echo getLetterGrade(74) ?></p>

</body>
</html>